#!/home/okami/python_projects/medical_management/bin/python
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
